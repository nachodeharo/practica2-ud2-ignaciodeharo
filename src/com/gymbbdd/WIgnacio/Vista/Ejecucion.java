package com.gymbbdd.WIgnacio.Vista;

/**
 *
 * Trabajo de bases de datos sobre mi tematica Clientes/Instructores, añadiendo el sistema MVC.
 * Mi aportación personal es:
 * - Creación de tabla de Registros con triggers de registro en Instructores y socios
 * - Botón de filtrado por dni con recarga de datos
 * - Añadida funcionalidad de boton desconectar que inhabilita los botones de la interfaz y vacía tablas
 * - Al añadir un Instructor preguntar contraseña
 * @author Nacho De Haro / SrWolfMoon
 * @see Controlador, Modelo, Vista, Util, Instalaciones, Tarifas
 * @version 24/01/2021   1.0
 * @since 1.8
 *
 */
public class Ejecucion {
    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Vista vista = new Vista();
        Controlador controlador = new Controlador(modelo, vista);
    }
}
